﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;




public class conversation : MonoBehaviour
{
    public GameObject[] text;
    public int sequence;
    public GameObject buttonNext, buttonPrev, btnChangeScene;
    public GameObject char1, char2;
    SpriteRenderer sr1, sr2;
    public transition trans;


    // Start is called before the first frame update
    void Start()
    {
        sequence = 0;
        sr1 = char1.GetComponent<SpriteRenderer>();
        sr2 = char2.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(sequence == 0 || sequence == 2)
        {
            
            sr2.color = Color.gray;
        }else
        {
            sr2.color = Color.white;
        }

        if (sequence == 1 || sequence == 3)
        {
            
            sr1.color = Color.gray;
        }
        else
        {
            sr1.color = Color.white;
        }

        if (sequence == 3)
        {
            buttonNext.SetActive(false);
            btnChangeScene.SetActive(true);
        }

        text[sequence].SetActive(true);
    }

    public void next()
    {
        text[sequence].SetActive(false);
        sequence += 1;
        text[sequence].SetActive(true);
    }

    public void prev()
    {
        text[sequence].SetActive(false);
        sequence -= 1;
        text[sequence].SetActive(true);

    }

    public void changeScene(string scene)
    {
        trans.changeScene(scene);
    }
}
