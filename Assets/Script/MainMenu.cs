﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MainMenu : MonoBehaviour
{
    public GameObject main, info, tentang,back, textInfo, textTentang;
    public transition trans;

    // Start is called before the first frame update
    void Start()
    {
        back.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void showTentang()
    {
        main.SetActive(false);
        info.SetActive(false);
       tentang.SetActive(false);
        textTentang.SetActive(true);
        back.SetActive(true);
    }

    public void showInfo()
    {
        main.SetActive(false);
        info.SetActive(false);
        tentang.SetActive(false);
        textInfo.SetActive(true);
        back.SetActive(true);
    }

    public void backButton()
    {
        main.SetActive(true);
        info.SetActive(true);
        tentang.SetActive(true);
        textInfo.SetActive(false);
        textTentang.SetActive(false);
        back.SetActive(false);
    }

    public void mulai()
    {
        trans.changeScene("Persiapan");
    }
}
