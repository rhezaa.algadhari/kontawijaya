﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleManager : MonoBehaviour
{
    public Player player;
    public Enemy enemy;
    string stance;
    public int level;
    string result;
    public Sprite rock, scissor, paper, menang,kalah;
    //bool kalah;
    public transition transition;
    public Text leveltext;
    public string nextSceneMenang, nextSceneKalah;
    public GameObject menangkalah;
    public Text namamusuh;

    // Start is called before the first frame update

    private void Awake()
    {
        if (level == 1)
        {

            PlayerPrefs.SetInt("ulti", 1);
        }
    }

    void Start()
    {
        stance = "begin";
              

        StartCoroutine("begin");
    }

    // Update is called once per frame
    void Update()
    {
        namamusuh.text = "" + enemy.nama;

        leveltext.text = "level " + level;

        if(stance == "selectHand")
        {
            for (int i = 0; i < player.handButton.Length; i++)
            {
                player.handButton[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < player.handButton.Length; i++)
            {
                player.handButton[i].SetActive(false);
            }
        }

        if(stance == "chooseSkill")
        {
            int u;

            if (player.haveUlti)
            {
                u = 3;
            }

            else
            {
                u = 2;

            }

            for (int i = 0; i < u; i++)
            {
               player.skillButton[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < player.skillButton.Length ; i++)
            {
                player.skillButton[i].SetActive(false);
            }
        }

       
        
    }

    public void selectHand(int number)
    {
        player.hand = number;
        stance = "battle";
        StartCoroutine("battle");
        enemy.selectHand();
    }

    public void selectSkill(int number)
    {
        player.hand = number;
        stance = "playanim";
        if (number == 3)
        {
            player.haveUlti = false;
            PlayerPrefs.SetInt("ulti", 0);

        }

        StartCoroutine("playanim");
    }

    IEnumerator begin()
    {
        stance = "begin";

        yield return new WaitForSeconds(2);

        stance = "selectHand";
        
    }

    IEnumerator battle()
    {
        yield return new WaitForSeconds(2);

        switch (player.hand)
        {
            case 1:
                player.handselection.GetComponent<SpriteRenderer>().sprite = rock;

                break;
            case 2:
                player.handselection.GetComponent<SpriteRenderer>().sprite = scissor;

                break;
            case 3:
                player.handselection.GetComponent<SpriteRenderer>().sprite = paper;

                break;
        }

        player.handselection.SetActive(true);

        switch (enemy.hand)
        {
            case 1:
                enemy.handselection.GetComponent<SpriteRenderer>().sprite = rock;

                break;
            case 2:
                enemy.handselection.GetComponent<SpriteRenderer>().sprite = scissor;

                break;
            case 3:
                enemy.handselection.GetComponent<SpriteRenderer>().sprite = paper;

                break;
        }

        enemy.handselection.SetActive(true);

        if(player.hand == enemy.hand)
        {
            result = "draw";

        } 
        else
        if ((player.hand == 1 && enemy.hand == 2) || (player.hand == 2 && enemy.hand == 3) || (player.hand == 3 && enemy.hand == 1))
        {
            result = "win";
           
        }
        else
        {
            result = "lose";
            
        }

        yield return new WaitForSeconds(2);
        player.handselection.SetActive(false);
        enemy.handselection.SetActive(false);

        if (result == "win")
        {
            stance = "chooseSkill";
        }
        else
        {
            stance = "playanim";
            StartCoroutine("playanim");
        }
    }

    IEnumerator playanim()
    {
        switch (result)
        {
            case "win":
                player.skill1(enemy, player.damage[player.hand],player.hand);
                
                yield return new WaitForSeconds(1);
                enemy.animator.SetInteger("state", 0);
                player.serangan[player.hand].SetActive(false);

                

                if(enemy.hp > 0)
                {
                    StartCoroutine("begin");
                }
                else
                {
                    //enemy.gameObject.SetActive(false);
                    enemy.animator.SetInteger("state", 2);
                    PlayerPrefs.SetString("result", "menang");
                    menangkalah.GetComponent<SpriteRenderer>().sprite = menang;
                    menangkalah.SetActive(true);
                    transition.changeScene(nextSceneMenang);
                }

                if (enemy.nama == "gatotkaca")                
                {
                    yield return new WaitForSeconds(1.5f);
                    enemy.heal(100);
                    yield return new WaitForSeconds(2.5f);
                    enemy.light.SetActive(false);
                }


                break;

            case "lose":
                enemy.skill1(player, enemy.damageSkill, 0);
                
                yield return new WaitForSeconds(1);
                player.animator.SetInteger("state", 0);
                enemy.serangan[0].SetActive(false);
                enemy.light.SetActive(false);
                if(player.hp > 0)
                {
                    StartCoroutine("begin");
                }
                else
                { 
                   // kalah = true;
                    player.animator.SetInteger("state", 2);
                    menangkalah.GetComponent<SpriteRenderer>().sprite = kalah;
                    menangkalah.SetActive(true);
                    PlayerPrefs.SetString("result", "kalah");
                    transition.changeScene(nextSceneKalah);
                }
                break;

            case "draw":
                yield return new WaitForSeconds(1);
                StartCoroutine("begin");
                break;

        }
    }

    

}
