﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Char
{
    public string nama;
    public GameObject light;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("state", 0);
    }

    // Update is called once per frame
    void Update()
    {
        sliderHP.value = hp;
    }

    public void selectHand()
    {
        int n = Random.Range(1, 3);

        hand = n;
    }

    public void heal(int heal)
    {
        hp = heal;
        light.SetActive(true);
    }

    public void skill1(Player player, int damage, int hand)
    {
        serangan[hand].SetActive(true);
        player.animator.SetInteger("state", 1);
        player.hp -= damage;

        if (nama == "gatotkaca")
        {
            heal(100);
        }
    }
}
