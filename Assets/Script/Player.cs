﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Char
{
    public GameObject[] skillButton;
    public GameObject[] handButton;
    public int[] damage;
    
    public bool haveUlti;
   
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.SetInteger("state", 0);

        if ((PlayerPrefs.GetInt("ulti")) == 0)
        {
            haveUlti = false;
        }
        else
        {
            haveUlti = true;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        sliderHP.value = hp;
        
    }

    public void skill1(Enemy enemy, int damage, int hand)
    {
        serangan[hand].SetActive(true);
        enemy.animator.SetInteger("state", 1);
        if (hand == 3)
        {
            enemy.hp = 0;
        }

        else
        {
            enemy.hp -= damage;
        }
    }

    
}
